defmodule Issues.GithubIssues do
  require Logger

  @user_agent [{"User-Agent", "benjaminboruff"}]
  @github_url Application.get_env(:issues, :github_url)

  def fetch(user, project) do
    Logger.debug("Fetching #{user}'s project #{project}' issues")

    issues_url(user, project)
    |> HTTPoison.get(@user_agent)
    |> handle_response
  end

  def issues_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/issues"
  end

  def handle_response({_, %{status_code: status_code, body: body}}) do
    Logger.debug("Got response: status code=#{status_code}")
    Logger.debug(fn -> inspect(body) end)

    {
      status_code |> check_status,
      body |> Poison.Parser.parse!(%{})
    }
  end

  defp check_status(200), do: :ok
  defp check_status(_), do: :error
end
