defmodule Issues.TableFormatter do
  def print_table_for_columns(rows, header, user, project) do
    table_title = "Issues for #{user}'s #{project} repo"
    issues = for issue <- rows, do: [issue["number"], issue["created_at"], issue["title"]]

    TableRex.quick_render!(issues, header, table_title)
    |> IO.puts()
  end
end
