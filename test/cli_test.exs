defmodule CLITest do
  use ExUnit.Case, async: true
  doctest Issues.CLI

  import Issues.CLI, only: [parse_args: 1, sort_into_descending_order: 1]

  test ":help should be returned by option parsing with -h and --help options" do
    assert parse_args(["-h", "foobar"]) == :help
    assert parse_args(["--help", "foobar"]) == :help
  end

  test "three values retuned if three given" do
    assert parse_args(["user", "project", "99"]) == {"user", "project", 99}
  end

  test "count is defaulted if only two values are given" do
    assert parse_args(["user", "project"]) == {"user", "project", 4}
  end

  test "sort descending orders correctly" do
    result = sort_into_descending_order(fake_created_at_list(["c", "a", "b"]))
    issues = for issue <- result, do: Map.get(issue, "created_at")
    assert issues == ~w{ c b a}
  end

  defp fake_created_at_list(values) do
    for value <- values, do: %{"created_at" => value, "other_stuff" => "asdf"}
  end
end
